## //This project is lab1 of PRN231 subject

Tech applying:

- UnitOfWork Design Pattern
- DI
- Docker deploy API

### how to using docker

Run Docker first
Step:

1. cd prn231_lab01_webappapi
1. docker compose up -d
1. URL: .../api/Products
